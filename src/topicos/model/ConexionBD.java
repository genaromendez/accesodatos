/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package topicos.model;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 *
 * @author gmendez
 */
public class ConexionBD {
    public com.mysql.jdbc.Connection conn;
    public com.mysql.jdbc.Statement  stat;
    public ResultSet  res;
 
    
    public ConexionBD(){        
        
    }
    
    public void conectar(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/musica?zeroDateTimeBehavior=convertToNull", "musica","musica");
            System.out.println("Conexión exitosa");
            
            this.stat = (Statement) this.conn.createStatement();
            
        } catch (Exception e){
            System.out.println("Error en iniciar conexión: "+e.getMessage());
        }
    }
}
